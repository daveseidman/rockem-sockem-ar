import React, { PropTypes, Component } from 'react';

export default class Button extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    socket: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    selected: PropTypes.bool,
  }

  static defaultProps = {
    selected: false,
  }

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { socket, index } = this.props;
    this.props.onClick(socket, index);
  }


  render() {
    const { name, index, selected } = this.props;
    const selectedClass = selected ? 'opponent-list-item-selected' : '';

    return (
      <li>
        <button
          className={`opponent-list-item ${selectedClass}`}
          index={index}
          onClick={this.onClick}
        >
          {name}
        </button>
      </li>
    );
  }
}
