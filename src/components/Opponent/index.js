import React, { PropTypes, Component } from 'react';
import Button from './modules/Button';
import './index.scss';

export default class Opponent extends Component {
  static propTypes = {
    status: PropTypes.string,
    active: PropTypes.bool,
    player: PropTypes.string,
    opponent: PropTypes.string,
    players: PropTypes.shape(),
    onClick: PropTypes.func.isRequired,
    invite: PropTypes.bool,
  };

  static defaultProps = {
    status: '',
    active: false,
    player: '',
    opponent: '',
    players: {},
    invite: false,
  };

  constructor(props) {
    super(props);

    this.getPlayer = this.getPlayer.bind(this);
    this.onClick = this.onClick.bind(this);

    this.state = {
      selected: -1,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.invite && !nextProps.invite) this.setState({ selected: -1 });
  }

  onClick(socket, index) {
    const { players } = this.props;
    this.setState({ selected: index });
    this.props.onClick(players[socket]);
  }

  getPlayer(key, index) {
    const { player, players } = this.props;
    const { selected } = this.state;
    const id = index;
    const opponent = players[key];
    if (opponent.socket === player || opponent.status === 'playing') return '';
    return (
      <Button
        name={opponent.name}
        selected={selected === index}
        index={id}
        key={`player-${id}`}
        socket={opponent.socket}
        onClick={this.onClick}
      />
    );
  }

  render() {
    const {
      active, players, opponent, status,
    } = this.props;

    const activeClass = active ? 'opponent-active' : '';

    return (
      <div className={`opponent ${activeClass}`}>
        <h1>Opponent: {status === 'playing' ? opponent : ''}</h1>
        <ul className="opponent-list">
          {
            Object.keys(players).map(this.getPlayer)
          }
        </ul>
      </div>
    );
  }
}
