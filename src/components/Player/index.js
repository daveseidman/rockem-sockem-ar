import React, { PropTypes, Component } from 'react';
import autoBind from 'react-autobind';
import './index.scss';

export default class Player extends Component {
  static propTypes = {
    player: PropTypes.string,
    status: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onComplete: PropTypes.func.isRequired,
  };

  static defaultProps = {
    player: '',
    status: '',
  }

  constructor(props) {
    super(props);
    autoBind(this);

    this.state = {
      active: true,
      issue: false,
    };
  }

  onkeydown(e) {
    this.setState({ issue: false });
    if (e.key === 'Backspace' || e.key === 'Shift') return;
    if (e.key === 'Enter') {
      const name = e.target.value;
      this.tryRegister(name);
    }
    const regex = new RegExp('^[a-zA-Z0-9]+$');
    const key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (!regex.test(key)) {
      e.preventDefault();
    }
  }

  onblur(e) {
    console.log('blurring', e);
  }

  tryRegister(name) {
    const { onSubmit, onComplete } = this.props;

    onSubmit(name).then((result) => {
      if (result) {
        this.setState({ active: false });
        onComplete(name);
      } else {
        this.setState({ issue: true });
      }
    });
  }

  render() {
    const { player, status } = this.props;
    const { active, issue } = this.state;

    const playerClass = active ? 'player-active' : '';
    const inputClass = status !== 'init' ? 'player-name-hidden' : '';
    const issueClass = issue ? 'player-issue-active' : '';

    return (
      <div className={`player ${playerClass}`}>
        <h1>Player: {player}</h1>
        <input
          type="text"
          className={`player-name ${inputClass}`}
          placeholder="Enter your name"
          pattern="[a-zA-Z0-9 ]+"
          onKeyDown={this.onkeydown}
          onBlur={this.onblur}
          disabled={!active}
        />
        <p className={`player-issue ${issueClass}`}>That name is taken</p>
      </div>
    );
  }
}
