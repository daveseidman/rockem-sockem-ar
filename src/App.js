import React, { Component } from 'react';
import autoBind from 'react-autobind';
import io from 'socket.io-client';
import './app.scss';
import Scene from './components/Scene';
import Controls from './components/Controls';
import Player from './components/Player';
import Invite from './components/Invite';
import Opponent from './components/Opponent';

// const io = require('socket.io-client');


// TODO: theres still a case where a player extends an invite, then a different
// opponent challenges the player, accepting that invite needs to revoke the
// origin invite from the player

// TODO: make socket internal somehow... this.socket?
let socket;

export default class App extends Component {
  constructor(props) {
    super(props);
    autoBind(this);

    socket = io.connect(NODE_ENV === 'dev' 
      ? '/'
      : 'https://rockem-sockem.brl-dev.com/');
    socket.on('connected', this.connected);
    socket.on('playerList', this.renderPlayerList);
    socket.on('matchRequested', this.matchRequested);
    socket.on('matchAccepted', this.matchAccepted);
    socket.on('matchRejected', this.matchRejected);
    socket.on('matchRevoked', this.matchRevoked);
    socket.on('opponentLeft', this.opponentLeft);
    socket.on('opponentMove', this.opponentMove);
    socket.on('opponentAction', this.opponentAction);

    // scene = new Scene(this.playerMove);
    // TODO: lets move this over to Redux once its settled
    this.state = {
      status: 'init',
      players: {},
      playerName: '',
      playerSocket: '',
      opponentName: '',
      opponentSocket: '',
      invite: false,
      inviteType: '',
      color: '',
    };
  }

  // ---------------------------------------------------------------------------
  // player actions:
  // ---------------------------------------------------------------------------

  // player has connected and been assigned a socket
  connected() {
    this.setState({ playerSocket: socket.id });
  }

  // check if player's name is available
  checkName(name) {
    return new Promise(((resolve, reject) => {
      fetch(`/api/signup/${name}/${this.state.playerSocket}`)
      .then(res => res.json())
      .then(({ success }) => resolve(success));
    }));
  }

  // name available: register player
  register(name) {
    this.setState({ status: 'registered', playerName: name });
  }

  // player is requesting a match
  requestMatch({ name, socket }) {
    // TODO: don't show opponent name until invite is accepted
    this.setState({
      invite: true, inviteType: 'sent', opponentName: name, opponentSocket: socket,
    });
    fetch(`/api/challenge/${this.state.playerSocket}/${socket}`);
  }

  // player is accepting match after invite from opponent
  acceptMatch() {
    fetch(`/api/accept/${this.state.opponentSocket}/${this.state.playerSocket}`);
    this.setState({ invite: false, status: 'playing', color: 'red' });
  }

  // player rejecting match after invite from opponent
  rejectMatch() {
    fetch(`/api/reject/${this.state.opponentSocket}`);
    this.setState({ opponentName: '', opponentSocket: '', invite: false });
  }

  // player revoking invitation for match with opponent
  revokeMatch() {
    fetch(`/api/revoke/${this.state.opponentSocket}`);
    this.setState({ opponentName: '', opponentSocket: '', invite: false });
  }

  // player moving (eventually split into move, attack, defend, etc)
  playerMove(pos, rot, dis) {
    socket.emit('playerMove', {
      opponent: this.state.opponentSocket, pos, rot, dis,
    });
  }

  playerAction(pressRelease, leftRight) {
    this.scene.playerAction(pressRelease, leftRight);
    socket.emit('playerAction', { opponent: this.state.opponentSocket, pressRelease, leftRight });
  }

  // ---------------------------------------------------------------------------
  // opponent actions
  // ---------------------------------------------------------------------------

  // opponent has requested match with player
  matchRequested({ name, socket }) {
    this.setState({
      invite: true, inviteType: 'recieved', opponentName: name, opponentSocket: socket,
    });
  }

  // opponent has accepted invite for match with player
  matchAccepted() {
    this.setState({ invite: false, status: 'playing', color: 'blue' });
  }

  // opponent has rejected invite for match with player
  matchRejected() {
    this.setState({ invite: false, opponentName: '', opponentSocket: '' });
  }

  // opponent has revoked invitation for match with player
  matchRevoked() {
    this.setState({ invite: false });
  }

  opponentLeft() {
    this.setState({ status: 'registered', opponentName: '', opponentSocket: '' });
  }
  // opponent moving
  opponentMove(data) {
    this.scene.opponentMove(data.dis);
  }

  opponentAction(data) {
    this.scene.opponentAction(data.pressRelease, data.leftRight);
  }

  // update player list
  renderPlayerList(players) {
    this.setState({ players });
  }


  render() {
    const {
      status,
      players,
      playerName,
      playerSocket,
      opponentName,
      invite,
      inviteType,
      color,
    } = this.state;

    return (
      <div className={`app app-${status}`}>
        <Scene
          playerMove={this.playerMove}
          status={status}
          color={color}
          ref={(scene) => { this.scene = scene; }}
        />
        <Controls
          active={status === 'playing'}
          playerAction={this.playerAction}
        />
        <Player
          player={playerName}
          status={status}
          onSubmit={this.checkName}
          onComplete={this.register}
        />
        <Opponent
          active={status === 'registered'}
          status={status}
          player={playerSocket}
          opponent={opponentName}
          players={players}
          invite={invite}
          onClick={this.requestMatch}
        />
        <Invite
          opponent={opponentName}
          active={invite}
          type={inviteType}
          accept={this.acceptMatch}
          reject={this.rejectMatch}
          revoke={this.revokeMatch}
        />
      </div>
    );
  }
}
