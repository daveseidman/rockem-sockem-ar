import React, { PropTypes, Component } from 'react';
import autoBind from 'react-autobind';
import { THREEx } from './modules/ar';
import './index.scss';

const THREE = require('three');


let arToolkitSource;
let arToolkitContext;

const container = new THREE.Object3D();
const scene = new THREE.Scene();
const origin = new THREE.Vector3();

window.scene = scene;
// window.origin = origin;

let renderer;
let camera;

let phone1;
// let's keep track of the last pos/rot we sent and only emit pos/rot data
// when there's been enough movement to minimize bandwidth
let sceneEl = null;

let player;
let opponent;
let playerLeft;
let playerRight;
let opponentLeft;
let opponentRight;
let playerLeftTarget = 0.1;
let playerRightTarget = 0.1;
let opponentLeftTarget = 0.1;
let opponentRightTarget = 0.1;
let redMat;
let blueMat;

const materials = {};


export default class Scene extends Component {
  static propTypes = {
    status: PropTypes.string.isRequired,
    playerMove: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
    autoBind(this);

    this.state = {
      playing: false,
    };

    renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true,
    });
    renderer.setClearColor(new THREE.Color('blue'), 0);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    const reflectionCube = new THREE.CubeTextureLoader().load(['assets/environment.jpg', 'assets/environment.jpg', 'assets/environment.jpg', 'assets/environment.jpg', 'assets/environment.jpg', 'assets/environment.jpg']);
    const bump = new THREE.TextureLoader().load('assets/ring-bump.jpg');
    const ringMat = new THREE.MeshPhongMaterial({
      color: 0xffe23e, specular: 0xfff9d4, shininess: 25, bumpMap: bump, bumpScale: 0.01,
    });
    const postsMat = new THREE.MeshPhongMaterial({ color: 0xffe23e, specular: 0xfff9d4, shininess: 25 });
    const ropesMat = new THREE.MeshPhongMaterial({ color: 0xfafafa, specular: 0xffffff, shininess: 5 });
    const botMat = new THREE.MeshStandardMaterial({
      color: 0x008080, roughness: 0.4, metalness: 0.8, envMap: reflectionCube, envMapIntensity: 1,
    });

    redMat = new THREE.MeshPhongMaterial({ color: 0xe4002b, specular: 0xffffff, shininess: 50 });
    blueMat = new THREE.MeshPhongMaterial({ color: 0x00899a, specular: 0xffffff, shininess: 50 });

    materials.red = redMat;
    materials.blue = blueMat;

    const sceneObjects = [
      {
        name: 'ring',
        mat: ringMat,
        geo: 'assets/ring.js',
      },
      {
        name: 'posts',
        mat: postsMat,
        geo: 'assets/posts.js',
      },
      {
        name: 'ropes',
        mat: ropesMat,
        geo: 'assets/ropes.js',
      },
      // },
      // {
      //   name: 'bot',
      //   mat: botMat,
      //   geo: './bot.js',
      // }
    ];

    sceneObjects.forEach((object) => {
      const objLoader = new THREE.JSONLoader();
      objLoader.load(object.geo, (geometry) => {
        const mesh = new THREE.Mesh(geometry, object.mat);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        mesh.scale.x = 0.1;
        mesh.scale.y = 0.1;
        mesh.scale.z = 0.1;
        container.add(mesh);
        // lift bot up to ring's floor
        if (object.name === 'bot') {
          mesh.position.set(-1, 0, 1.5);
          mesh.rotation.y = 0.785398;
        }
      });
    });

    const phoneLoader = new THREE.JSONLoader();
    phoneLoader.load('assets/phone.js', (geo) => {
      phone1 = new THREE.Mesh(geo, botMat);
      phone1.castShadow = true;
      phone1.receiveShadow = true;
      phone1.scale.x = 0.05;
      phone1.scale.y = 0.05;
      phone1.scale.z = 0.05;
    });

    player = new THREE.Mesh(
      new THREE.BoxGeometry(0.2, 0.5, 0.2),
      redMat
    );
    player.position.set(0.33, 0.25, 0.33);
    player.rotation.y = 2.35619;
    player.name = 'player';
    player.castShadow = true;
    player.receiveShadow = true;

    playerLeft = new THREE.Mesh(new THREE.BoxGeometry(0.4, 0.075, 0.075), redMat);
    playerLeft.position.set(0.1, 0.1, -0.15);
    playerLeft.name = 'left';
    playerLeft.castShadow = true;
    playerLeft.receiveShadow = true;
    player.add(playerLeft);

    playerRight = new THREE.Mesh(new THREE.BoxGeometry(0.4, 0.075, 0.075), redMat);
    playerRight.position.set(0.1, 0.1, 0.15);
    playerRight.name = 'right';
    playerRight.castShadow = true;
    playerRight.receiveShadow = true;
    player.add(playerRight);

    container.add(player);

    opponent = new THREE.Mesh(new THREE.BoxGeometry(0.2, 0.5, 0.2), blueMat);
    opponent.position.set(-0.33, 0.25, -0.33);
    opponent.rotation.y = 5.49779;
    opponent.name = 'opponent';
    opponent.castShadow = true;
    opponent.receiveShadow = true;
    opponentLeft = new THREE.Mesh(new THREE.BoxGeometry(0.4, 0.075, 0.075), blueMat);
    opponentLeft.position.set(0.1, 0.1, -0.15);
    opponentLeft.name = 'left';
    opponentLeft.castShadow = true;
    opponentLeft.receiveShadow = true;
    opponent.add(opponentLeft);

    opponentRight = new THREE.Mesh(new THREE.BoxGeometry(0.4, 0.075, 0.075), blueMat);
    opponentRight.position.set(0.1, 0.1, 0.15);
    opponentRight.name = 'right';
    opponentRight.castShadow = true;
    opponentRight.receiveShadow = true;
    opponent.add(opponentRight);

    container.add(opponent);

    const light1 = new THREE.SpotLight(0xffffff, 1.1, 500);
    light1.position.set(50, 200, 80);
    light1.castShadow = true;
    light1.angle = 0.25;
    light1.shadow.mapSize.width = 512;
    light1.shadow.mapSize.height = 512;
    light1.shadow.camera.near = 50;
    light1.shadow.camera.far = 300;
    light1.shadow.bias = 0.0001;
    container.add(light1);

    const light2 = new THREE.SpotLight(0xffffff, 1.1, 500);
    light2.position.set(-50, 200, -80);
    light2.castShadow = true;
    light2.angle = 0.25;
    light2.shadow.mapSize.width = 512;
    light2.shadow.mapSize.height = 512;
    light2.shadow.camera.near = 50;
    light2.shadow.camera.far = 300;
    light2.shadow.bias = 0.0001;
    container.add(light2);

    // TODO: do this in 3ds
    container.scale.x = .75;
    container.scale.y = .75;
    container.scale.z = .75;
    container.position.y = .2;
    scene.add(container);

    this.player = player;
    this.opponent = opponent;
  }

  shouldComponentUpdate(nextProps) {
    return !(this.state.playing || nextProps === this.props || nextProps.status !== 'playing');
  }

  componentWillUpdate(nextProps) {
    if (this.state.playing) return;

    if (nextProps.status === 'playing') this.play();
  }

  componentDidUpdate() {
    const { color } = this.props;
    const other = color === 'blue' ? 'red' : 'blue';

    if (color === 'red') container.rotation.y = 3.14159;

    player.material = materials[color];
    playerLeft.material = materials[color];
    playerRight.material = materials[color];
    opponent.material = materials[other];
    opponentLeft.material = materials[other];
    opponentRight.material = materials[other];
  }

  // TODO: combine these, split on player/opponent
  playerMove(dis) {
    player.position.x = dis / 10;
    player.position.z = dis / 10;
  }
  opponentMove(dis) {
    this.opponent.position.x = -dis / 10;
    this.opponent.position.z = -dis / 10;
  }

  playerAction(pressRelease, leftRight) {
    if (leftRight === 'left') {
      playerLeftTarget = pressRelease === 'press' ? 0.2 : 0.1;
    } else {
      playerRightTarget = pressRelease === 'press' ? 0.2 : 0.1;
    }
  }
  opponentAction(pressRelease, leftRight) {
    if (leftRight === 'left') {
      opponentLeftTarget = pressRelease === 'press' ? 0.2 : 0.1;
    } else {
      opponentRightTarget = pressRelease === 'press' ? 0.2 : 0.1;
    }
  }

  play() {
    this.setState({ playing: true });
    sceneEl.appendChild(renderer.domElement);
    camera = new THREE.Camera();
    scene.add(camera);
    arToolkitSource = new THREEx.ArToolkitSource({
      sourceType: 'webcam',
    });
    // try to init AR,
    // if it works, render the AR camera, if not, render a standard camera
    arToolkitSource.init(this.arSuccess, this.arError);
  }


  arSuccess() {
    arToolkitContext = new THREEx.ArToolkitContext({
      cameraParametersUrl: 'assets/camera_para.dat',
      detectionMode: 'mono',
    });

    arToolkitContext.init(() => {
      camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());
    });

    const markerControls = new THREEx.ArMarkerControls(arToolkitContext, camera, {
      type: 'pattern',
      patternUrl: 'assets/test2.patt',
      changeMatrixMode: 'cameraTransformMatrix',
    });

    window.addEventListener('resize', this.playerResize);

    this.playerResize();
    this.animate();
  }

  arError() {
    camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 1, 10000);
    scene.add(camera);
    camera.position.set(3, 2.5, -5);
    camera.lookAt(origin);
    window.camera = camera;
    window.addEventListener('resize', this.watchResize);
    this.watchResize();
    this.animate();
  }


  animate() {
    if (arToolkitSource.ready)	{
      arToolkitContext.update(arToolkitSource.domElement);

      const pos = camera.position;
      const rot = camera.rotation;
      const dis = pos.distanceTo(origin);

      this.playerMove(dis);
      this.props.playerMove(pos, rot, dis);
    }

    // move arms
    playerLeft.position.x += (playerLeftTarget - playerLeft.position.x) / 2;
    playerRight.position.x += (playerRightTarget - playerRight.position.x) / 2;
    opponentLeft.position.x += (opponentLeftTarget - opponentLeft.position.x) / 2;
    opponentRight.position.x += (opponentRightTarget - opponentRight.position.x) / 2;


    renderer.render(scene, camera);

    requestAnimationFrame(this.animate);
  }


  playerResize() {
    arToolkitSource.onResize();
    arToolkitSource.copySizeTo(renderer.domElement);
    if (arToolkitContext.arController !== null) {
      arToolkitSource.copySizeTo(arToolkitContext.arController.canvas);
    }
  }


  watchResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }


  render() {
    console.log('should only render once');
    return (
      <div className="scene" ref={(el) => { sceneEl = el; }} />
    );
  }
}

