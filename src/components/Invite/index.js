import React, { PropTypes, Component } from 'react';
import './index.scss';

export default class Invite extends Component {
  static propTypes = {
    active: PropTypes.bool,
    opponent: PropTypes.string,
    type: PropTypes.string,
    accept: PropTypes.func.isRequired,
    reject: PropTypes.func.isRequired,
    revoke: PropTypes.func.isRequired,
  }

  static defaultProps = {
    active: false,
    opponent: '',
    type: '',
  };

  componentWillUpdate(nextProps) {
  // TODO: check for the update where we go from having an opponent to not
  // and display an 'opponent has left' message
  //  console.log('component updating', this.props.opponent, nextProps.opponent);
  }

  render() {
    const {
      active,
      opponent,
      type,
      accept,
      reject,
      revoke,
    } = this.props;

    const activeClass = active ? 'invite-active' : '';

    return (
      <div className={`invite ${activeClass}`}>
        <div className="invite-message">
          <p>{`You ${type} an invite ${type === 'sent' ? 'to' : 'from'} ${opponent}`}</p>
        </div>
        {
            type === 'sent' ?
              <div className="invite-actions">
                <button
                  className="invite-actions-revoke"
                  onClick={revoke}
                >
                  Revoke Invitation
                </button>
              </div>
              :
              <div className="invite-actions">
                <button
                  className="invite-actions-accept"
                  onClick={accept}
                >
                  Accept Invitation
                </button>
                <button
                  className="invite-actions-reject"
                  onClick={reject}
                >
                  Reject Invitation
                </button>
              </div>
          }
      </div>
    );
  }
}
