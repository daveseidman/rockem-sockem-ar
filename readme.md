# Rock'em Sock'em Robots AR Game

Classic board game from the 1960's rebooted in AR.
Test of multiplayer AR using latest available technology.

---

## Development

Run these two scripts in separate terminals:

`npm run build-dev` 
will start webpack-dev-server which serves and hot reloads client scripts

`npm run serve-dev` 
will start an express server with socket.io

View the app in your browser at http://localhost:8080


## Production

Run the first script and then the second

`npm run build-prod`
will run webpack then copy everything from the assets to dist folder

`npm run serve-prod`
will start and express server with socket.io

view the app in your browser at http://localhost:8000


### Note, to get live camera in browser most devices require SSL:

Install and Configure nginx or apache, setup an SSL, and forward http and ssl
traffic to whichever port you're running the app on.

```
server {
    listen       8888;
    listen       443 ssl;
    ssl_certificate      /etc/ssl/certs/myssl.crt;
    ssl_certificate_key  /etc/ssl/private/myssl.key;
    server_name  localhost;

    location / {
        proxy_pass https://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```


### Gameplay:




### Workflow for exporting from Max to Three.js:

1. Export Selected as a .obj file using these settings:  
![alt text](static/export-settings.jpg "OBJ settings")

2. Convert to .js using the convert_obj_three.py script:
`python ./convert_obj_three.py -i input-file.obj -o output-file.js`

3. Load into scene using the THREE.JSONLoader

Materials are being created with THREE.js since they don't convert well 
but it's important to export models with UVW Maps as those are retained.





### Credits / Inspo:

AR.js from Jerome Etienne: https://github.com/jeromeetienne/AR.js
Working demo for Tanks Multiplayer: https://youtu.be/oNekBgognFE?t=45m41s
