const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const entry = process.env.NODE_ENV === 'dev'
  ? ['react-hot-loader/patch','webpack-hot-middleware/client','./src/index']
  : ['./src/index']

module.exports = {
  entry: entry,
  devtool: 'inline-source-map',
  devServer: {
    https: true,
    proxy: {
      '/api': 'http://[::1]:8000'
    }
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({ NODE_ENV: process.env.NODE_ENV }),
    new CleanWebpackPlugin(['dist']),
    new CopyWebpackPlugin([{ from: 'assets/**/*' }]),
    new HtmlWebpackPlugin({ title: 'Rock\'em Sock\'em Robots!', template: 'src/index-template.html' }),
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel-loader'],
      include: path.join(__dirname, 'src'),
      exclude: path.join(__dirname, 'src/components/Scene/modules/ar.js'),
    }, {
      test: /\.scss$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader'],
    }],
  },
};
