import React, { PropTypes, Component } from 'react';
import autoBind from 'react-autobind';

import './index.scss';

// TODO: should be stateless

export default class Controls extends Component {
  static propTypes = {
    playerAction: PropTypes.func.isRequired,
    active: PropTypes.bool,
  };

  static defaultProps = {
    active: false,
  }

  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active) {
      document.addEventListener('keydown', this.keydown);
      document.addEventListener('keyup', this.keyup);
    } else {
      document.removeEventListener('keydown', this.keydown);
      document.removeEventListener('keyup', this.keyup);
    }
  }

  keydown(e) {
    if (e.key === 'ArrowLeft') this.props.playerAction('press', 'left');
    if (e.key === 'ArrowRight') this.props.playerAction('press', 'right');
  }

  keyup(e) {
    if (e.key === 'ArrowLeft') this.props.playerAction('release', 'left');
    if (e.key === 'ArrowRight') this.props.playerAction('release', 'right');
  }

  touchstart(e) {
    if (e.target.className === 'controls-left') this.props.playerAction('press', 'left');
    if (e.target.className === 'controls-right') this.props.playerAction('press', 'right');
  }

  touchend(e) {
    if (e.target.className === 'controls-left') this.props.playerAction('release', 'left');
    if (e.target.className === 'controls-right') this.props.playerAction('release', 'right');
  }


  render() {
    const { active, playerAction } = this.props;
    const activeClass = active ? 'controls-active' : '';

    return (
      <div className={`controls ${activeClass}`}>
        <button
          onTouchStart={this.touchstart}
          onTouchEnd={this.touchend}
          className="controls-left"
        >
          Left Punch
        </button>
        <button
          onTouchStart={this.touchstart}
          onTouchEnd={this.touchend}
          className="controls-right"
        >
          Right Punch
        </button>
      </div>
    );
  }
}
