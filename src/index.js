import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import App from './App';

// for production build we do not need the appcontainer which provides HMR
const render = NODE_ENV === 'dev'
? (Component) => {
    ReactDOM.render(
      <AppContainer>
        <Component />
      </AppContainer>,
      document.getElementById('root')
    );
  } 
: (Component) => {
    ReactDOM.render(
        <Component />,
      document.getElementById('root')
    );
  }

render(App);
if (module.hot) module.hot.accept('./App', () => render(App));
