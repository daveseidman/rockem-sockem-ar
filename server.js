const fs = require('fs');
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
// const https = require('https');
// const privateKey = fs.readFileSync('/etc/ssl/private/myssl.key', 'utf8');
// const certificate = fs.readFileSync('/etc/ssl/certs/myssl.crt', 'utf8');
// const credentials = { key: privateKey, cert: certificate };
// const server = https.createServer(credentials, app);
const api = require('./api')(server);
const port = 8000;

if (process.env.NODE_ENV === 'dev') {
  const config = require('./webpack.config');
  const webpack = require('webpack');
  const compiler = webpack(config);
  app.use(require('webpack-dev-middleware')(compiler, { publicPath: config.output.path }));
  app.use(require('webpack-hot-middleware')(compiler));
}

app.use(express.static('dist'));
app.get('/api/signup/:name/:socket', api.signup);
app.get('/api/challenge/:player/:opponent', api.challenge);
app.get('/api/accept/:player1/:player2', api.accept);
app.get('/api/reject/:player1', api.reject);
app.get('/api/revoke/:player1', api.revoke);
server.listen(port, () => { console.log(`secure server listening on port ${port}`); });

