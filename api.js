module.exports = function(server)  {

  const api = {};
  const io = require('socket.io')(server);
  const players = {};

  io.on('connection', (socket) => {
    socket.emit('connected');
    socket.emit('playerList', players);
    socket.on('disconnect', () => { api.leave(socket); });
    socket.on('playerMove', api.playerMove);
    socket.on('playerAction', api.playerAction);
  });

  // utility function
  const playerExists = (name) => {
    for (const key in players) {
      const player = players[key];
      if (player.name === name) return true;
    }
    return false;
  };

  // player is signing up, make sure their name is unique
  api.signup = (req, res) => {
    const name = req.params.name.toLowerCase();
    const { socket } = req.params;

    // player name already exists, send error
    if (playerExists(name)) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      return res.send(JSON.stringify({ success: false }));
    }

    // add player to array
    players[socket] = { socket, name, status: 'waiting' };

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify({ success: true }));

    // tell all clients to update their player list
    io.emit('playerList', players);
  };

  // player is challenging an opponent
  api.challenge = (req, res) => {
    io.to(req.params.opponent).emit('matchRequested', players[req.params.player]);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify({ success: true }));
  };

  // player is accepting challenge from an opponent
  api.accept = (req, res) => {
    players[req.params.player1].status = 'playing';
    players[req.params.player2].status = 'playing';
    players[req.params.player1].opponent = req.params.player2;
    players[req.params.player2].opponent = req.params.player1;
    io.emit('playerList', players);

    io.to(req.params.player1).emit('matchAccepted');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify({ success: true }));
  };

  // player is rejecting challenge from an opponent
  api.reject = (req, res) => {
    io.to(req.params.player1).emit('matchRejected');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify({ success: true }));
  };

  // player is revoking their invitation to an opponent
  api.revoke = (req, res) => {
    io.to(req.params.player1).emit('matchRevoked');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify({ success: true }));
  };

  // player has left (refreshed, url changed or tab closed)
  api.leave = (socket) => {
    if (players[socket.id]) {
      io.to(players[socket.id].opponent).emit('opponentLeft');
    }
    delete players[socket.id];
    io.emit('playerList', players);
  };

  // recieved player position from client
  // this is the main engine of the socket server
  api.playerMove = (data) => {
    // TODO: remove position rotation data to increase performance if not necessary
    io.to(data.opponent).volatile.emit('opponentMove', { pos: data.pos, rot: data.rot, dis: data.dis });
  };

  api.playerAction = (data) => {
    io.to(data.opponent).emit('opponentAction', { pressRelease: data.pressRelease, leftRight: data.leftRight });
  };

  return api;
}